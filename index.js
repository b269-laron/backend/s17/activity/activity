/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function yourInfo(){
	alert("Hi, please answer the following questions");
	let fullName = prompt("What is your full name?");
	let age = prompt("What is your age?");
	let location = prompt("Where is your location");

	console.log("Hello, " + fullName);
	console.log("You are " + age + " years old.");
	console.log("You live in " + location);
}
yourInfo();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function myFavoriteBands (){
	console.log("1. Westlife");
	console.log("2. One Directon");
	console.log("3. 98 Degrees");
	console.log("4. N'Sync");
	console.log("5. Backstreetboys");
}

myFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function myTopMovies (){
	console.log("A.I.: Artificial Intelligence")
	console.log("Rottem Tomatoes Rating: 75%")
	console.log("Monte Carlo")
	console.log("Rottem Tomatoes Rating: 40%")
	console.log("Hannah Montana: The Movie")
	console.log("Rottem Tomatoes Rating: 43%")
	console.log("Camp Rock")
	console.log("Rottem Tomatoes Rating: 48%")
	console.log("Camp Rock 2: The Final Jam")
	console.log("Rottem Tomatoes Rating: 63%")
}

myTopMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


/*let printFriends =*/ function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}

printUsers();
